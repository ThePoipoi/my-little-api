package fr.antoine.mylittleapi;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.antoine.mylittleapi.model.Folder;
import fr.antoine.mylittleapi.model.Message;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//@WebMvcTest(MainController.class)
@SpringBootTest
@AutoConfigureMockMvc
class MessageControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;


    @Test
    public void innsoScenario() throws Exception {
        String senderNameOne = "Jérémie Durand";
        String contentOne = "Bonjour, j’ai un problème avec mon nouveau téléphone";
        String senderNameTwo = "Sonia Valentin";
        String contentTwo = "Je suis Sonia, et je vais mettre tout en oeuvre pour vous aider. " +
                "Quel est le modèle de votre téléphone ?";
        String reference = "KA-18B6";

        System.out.println("======= STEP 1 =======");
        MvcResult result = mockMvc.perform(post("/message")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(new Message(senderNameOne, contentOne))))
                .andExpect(status().isCreated())
                .andReturn();
        System.out.println("Résultat =======>");
        System.out.println(result.getResponse().getContentAsString(StandardCharsets.UTF_8));

        Assertions.assertThat(result.getResponse().getContentAsString(StandardCharsets.UTF_8))
                .contains("\"id\":")
                .contains("\"senderName\":\""+senderNameOne);

        System.out.println("======= STEP 2 =======");
        result = mockMvc.perform(post("/folder?")
                        .param("customerName", senderNameOne))
                .andExpect(status().isCreated())
                .andReturn();
        System.out.println("Résultat =======> " + result.getResponse().getContentAsString(StandardCharsets.UTF_8));

        Assertions.assertThat(result.getResponse().getContentAsString(StandardCharsets.UTF_8))
                .contains("\"id\":")
                .contains("\"customerName\":\""+senderNameOne)
                .contains("\"reference\":null");

        System.out.println("======= STEP 3 =======");
        // Création du message complet à partir du retour de la création du folder
        Folder folder = objectMapper.readValue(result.getResponse().getContentAsString(StandardCharsets.UTF_8), Folder.class);
        Message messageTwo = new Message(senderNameTwo, contentTwo, null, folder);
        result = mockMvc.perform(post("/message")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(messageTwo)))
                .andExpect(status().isCreated())
                .andReturn();
        System.out.println("Résultat =======> " + result.getResponse().getContentAsString(StandardCharsets.UTF_8));

        Assertions.assertThat(result.getResponse().getContentAsString(StandardCharsets.UTF_8))
                .contains("\"id\":")
                .contains("\"senderName\":\""+senderNameTwo)
                .contains("\"customerName\":\""+senderNameOne)
                .contains("\"reference\":null");

        System.out.println("======= STEP 4 =======");
        // Modif de la référénce directement sur l'objet
        folder.setReference(reference);
        result = mockMvc.perform(put("/folder")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(folder)))
                .andExpect(status().isOk())
                .andReturn();
        System.out.println("Résultat =======> " + result.getResponse().getContentAsString(StandardCharsets.UTF_8));

        Assertions.assertThat(result.getResponse().getContentAsString(StandardCharsets.UTF_8))
                .contains("\"id\":")
                .contains("\"customerName\":\""+senderNameOne)
                .contains("\"reference\":\""+reference);

        System.out.println("======= STEP 5 =======");
        result = mockMvc.perform(get("/folder"))
                .andExpect(status().isOk())
                .andReturn();
        System.out.println("Résultat =======> " + result.getResponse().getContentAsString(StandardCharsets.UTF_8));

        List<Folder> folders = objectMapper.readValue(result.getResponse().getContentAsString(StandardCharsets.UTF_8),
                new TypeReference<List<Folder>>(){});
        Assertions.assertThat(folders.size()).isEqualTo(1);
    }
}