package fr.antoine.mylittleapi.model;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "FOLDER")
public class Folder {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "Id", nullable = false)
	private Long id;

	@Column(name = "Customer_name", length = 64, nullable = false)
	private String customerName;

	@Column(name = "Opening_Date", nullable = false)
	private Timestamp openingDate;

	@Column(name = "Reference", length = 10)
	private String reference;

	public Folder() {};

	public Folder(String customerName, String reference) {
		this.customerName = customerName;
		this.reference = reference;
		this.openingDate = new Timestamp(System.currentTimeMillis());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Timestamp getOpeningDate() {
		return openingDate;
	}

	public void setOpeningDate(Timestamp openingDate) {
		this.openingDate = openingDate;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	@Override
	public String toString() {
		return "Folder{" +
				"id=" + id +
				", customerName='" + customerName + '\'' +
				", openingDate=" + openingDate +
				", reference='" + reference + '\'' +
				'}';
	}
}
