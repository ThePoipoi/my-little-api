package fr.antoine.mylittleapi.model;

public enum Channel {
	MAIL,
	SMS,
	FACEBOOK,
	TWITTER;
}
