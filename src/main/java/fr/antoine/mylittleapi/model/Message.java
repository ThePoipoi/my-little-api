package fr.antoine.mylittleapi.model;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "MESSAGE")
public class Message {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "Id", nullable = false)
	private Long id;

	@Column(name = "Sender_Name", length = 64, nullable = false)
	private String senderName;

	@Column(name = "Sending_Date", nullable = false)
	private Timestamp sendingDate;

	@Column(name = "Content", length = 1024, nullable = false)
	private String content;

	@Column(name = "Channel")
	private Channel channel;

	@ManyToOne(targetEntity=Folder.class, fetch=FetchType.EAGER)
	private Folder folder;

	public Message() {};

	public Message(String senderName, String content) {
		this(senderName, content, null);
	}

	public Message(String senderName, String content, Channel channel) {
		this(senderName, content, channel, null);
	}

	public Message(String senderName, String content, Channel channel, Folder folder) {
		this.senderName = senderName;
		this.content = content;
		this.channel = channel;
		this.folder = folder;
		this.sendingDate = new Timestamp(System.currentTimeMillis());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public Timestamp getSendingDate() {
		return sendingDate;
	}

	public void setSendingDate(Timestamp sendingDate) {
		this.sendingDate = sendingDate;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Channel getChannel() {
		return channel;
	}

	public void setChannel(Channel channel) {
		this.channel = channel;
	}

	public Folder getFolder() {
		return folder;
	}

	public void setFolder(Folder folder) {
		this.folder = folder;
	}

	@Override
	public String toString() {
		return "Message{" +
				"id=" + id +
				", senderName='" + senderName + '\'' +
				", sendingDate=" + sendingDate +
				", content='" + content + '\'' +
				", channel=" + channel +
				", folder=" + folder +
				'}';
	}
}