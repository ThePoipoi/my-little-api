package fr.antoine.mylittleapi.repository;

import fr.antoine.mylittleapi.model.Message;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;


public interface MessageRepository extends CrudRepository<Message, Long> {

	@Query(value = "SELECT * FROM MESSAGE WHERE SENDER_NAME = ? AND FOLDER_ID IS NULL", nativeQuery = true)
	Iterable<Message> findAllOrphansBySenderName(String senderName);

}
