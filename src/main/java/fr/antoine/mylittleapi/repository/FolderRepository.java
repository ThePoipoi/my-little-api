package fr.antoine.mylittleapi.repository;

import fr.antoine.mylittleapi.model.Folder;
import org.springframework.data.repository.CrudRepository;

public interface FolderRepository extends CrudRepository<Folder, Long> {

}
