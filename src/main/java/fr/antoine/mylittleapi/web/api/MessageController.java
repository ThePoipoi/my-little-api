package fr.antoine.mylittleapi.web.api;

import fr.antoine.mylittleapi.model.Message;
import fr.antoine.mylittleapi.repository.FolderRepository;
import fr.antoine.mylittleapi.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.Optional;

@RestController
@RequestMapping(value = "/message")
public class MessageController {

	@Autowired
	private MessageRepository messageRepository;
	@Autowired
	private FolderRepository folderRepository;

	/**
	 * Permet de récupérer un message via son id
	 * @param id l'id du message
	 * @return le message s'il existe, null sinon (not_found)
	 */
	@RequestMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Message> getMessage(@PathVariable("id") Long id) {
		Optional<Message> message = messageRepository.findById(id);
		if (!message.isPresent()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(message.get(), HttpStatus.OK);
	}

	/**
	 * Permet de créer un message
	 * @param message le message
	 * @return le message
	 */
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Message> createMessage(@RequestBody Message message) {
		if (message.getSendingDate() == null) {
			message.setSendingDate(new Timestamp(System.currentTimeMillis()));
		}
		messageRepository.save(message);
		return new ResponseEntity<>(message, HttpStatus.CREATED);
	}

	/**
	 * Permet de modifier un message
	 * @param message le message
	 * @return le message modifié
	 */
	@RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Message> updateMessage(@RequestBody Message message) {
		messageRepository.save(message);
		return new ResponseEntity<>(message, HttpStatus.CREATED);
	}

	/**
	 * Permet de récupérer l'ensemble des messages
	 * @return les messages de la base
	 */
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Iterable<Message>> getMessages() {
		Iterable<Message> messages = messageRepository.findAll();
		return new ResponseEntity<Iterable<Message>>(messages, HttpStatus.OK);
	}

}