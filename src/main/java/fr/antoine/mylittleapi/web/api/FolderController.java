package fr.antoine.mylittleapi.web.api;

import fr.antoine.mylittleapi.model.Folder;
import fr.antoine.mylittleapi.model.Message;
import fr.antoine.mylittleapi.repository.FolderRepository;
import fr.antoine.mylittleapi.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(value = "/folder")
public class FolderController {

	@Autowired
	private MessageRepository messageRepository;
	@Autowired
	private FolderRepository folderRepository;

	/**
	 * Permet de récupérer un folder via son id
	 * @param id l'id du folder
	 * @return le folder s'il existe, null sinon (not_found)
	 */
	@RequestMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Folder> getFolder(@PathVariable("id") Long id) {
		Optional<Folder> folder = folderRepository.findById(id);
		return !folder.isPresent() ? new ResponseEntity<>(HttpStatus.NOT_FOUND) : new ResponseEntity<>(folder.get(), HttpStatus.OK);
	}

	/**
	 * Permet de créer un dossier à partir d'un customerName et (optionnellement) d'une référence
	 * @param customerName le nom du client
	 * @param reference le numéro de ref
	 * @return le folder
	 */
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Folder> createFolder(@RequestParam String customerName , @RequestParam(required = false) String reference) {
		Folder folder = new Folder(customerName, reference);
		folder = folderRepository.save(folder);
		// Une fois le dossier créé, on va le rattacher à tous les messages de client du même nom et sans dossier
		Iterable<Message> messages = messageRepository.findAllOrphansBySenderName(customerName);
		for (Message message : messages) {
			message.setFolder(folder);
			messageRepository.save(message);
		}
		return new ResponseEntity<Folder>(folder, HttpStatus.CREATED);
	}

	/**
	 * Permet de modifier un folder
	 * @param folder le folder
	 * @return le folder modifié
	 */
	@RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Folder> updateFolder(@RequestBody Folder folder) {
		folderRepository.save(folder);
		return new ResponseEntity<Folder>(folder, HttpStatus.OK);
	}

	/**
	 * Permet de récupérer l'ensemble des folders
	 * @return les folders de la base
	 */
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Iterable<Folder>> getFolders() {
		Iterable<Folder> folders = folderRepository.findAll();
		return new ResponseEntity<Iterable<Folder>>(folders, HttpStatus.OK);
	}

}
