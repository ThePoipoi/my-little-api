package fr.antoine.mylittleapi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyLittleApiApplication {

	Logger logger = LoggerFactory.getLogger(MyLittleApiApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(MyLittleApiApplication.class, args);
	}

}
